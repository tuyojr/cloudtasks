#

![Altschool Africa](https://github.com/tuyojr/cloudTasks/blob/main/logos/AltSchool.svg)

## What is it?

An APT repository is a network server or a local directory containing deb packages and metadata files that are readable by the APT tools. `add-apt-repository` is a Python script that allows you to add an APT repository to either `/etc/apt/sources.list` or to a separate file in the `/etc/apt/sources.list.d` directory. The command can also be used to remove an already existing repository.

## How to use it

The first method to add it is by using the add-apt-repository command and the second one is to manually add the repository using a text editor.

On Ubuntu and all other Debian based distributions, the apt software repositories are defined in the `/etc/apt/sources.list` file or in separate files under the `/etc/apt/sources.list.d/` directory.

The names of the repository files inside the `/etc/apt/sources.list.d/` directory must end with `.list`.

To be able to add or remove a repository you need to be logged in as either a user with sudo access or root. The add-apt-repository utility is included in the software-properties-common package. To install it run the following commands:

```Linux CLI
sudo apt update
sudo apt install software-properties-common
```

The basic syntax of the `add-apt-repository` command is as follows:

```Linux CLI
add-apt-repository [options] repository
```

Where `repository` can be either a regular repository entry that can be added to the `sources.list` file like `deb <http://repo.tld/ubuntu> distro component` or a PPA repository in the `ppa:<user>/<ppa-name>` format.

To see all available options of the `add-apt-repository` command type `man add-apt-repository` in your terminal.

&copy;[Linuxize](https://linuxize.com/post/how-to-add-apt-repository-in-ubuntu/)
