#

![Altschool Africa](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/logos/AltSchool.svg)

## Exercise

- Create 3 groups – admin, support & engineering and add the admin group to sudoers.
- Create a user in each of the groups.
- Generate SSH keys for the user in the admin group

>Instruction:
>Submit the contents of /etc/passwd, /etc/group, /etc/sudoers

## SOLUTION

```LINUX COMMAND
#adminUser
ssh-keygen
```

![adminUser-ssh-keygen](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise3/images/adminUser-ssh-keygen.png)

```LINUX COMMAND
#checking created groups
cat /etc/group
```

![cat /etc/group](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise3/images/cat-etc-group.png)

```LINUX COMMAND
#checking created users
cat /etc/passwd
```

![cat /etc/passwd](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise3/images/cat-etc-passwd.png)

```LINUX COMMAND
#checking if admin is added to sudoers
cat /etc/sudoers
```

![cat /etc/passwd](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise3/images/cat-etc-sudoers.png)
