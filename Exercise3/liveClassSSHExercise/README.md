#

![Altschool Africa](https://github.com/tuyojr/cloudTasks/blob/main/logos/AltSchool.svg)

## Exercise

Create two Vagrant systems within the same private IP and allow ssh access from SystemA to SystemB. Should be able to login to SystemB from SystemA.
