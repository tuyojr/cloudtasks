#

![Altschool Africa](https://gitlab.com/tuyojr/cloudtasks/-/main/Exercise2/images/AltSchool.svg)

## Exercise 2

Task: Research online for 10 more linux commands aside the ones already mentioned in this module. Submit using your altschool-cloud-exercises project, explaining what each command is used for with examples of how to use each and example screenshots of using each of them.

>Instruction: Submit your work in a folder for this exercise in your altschool-cloud-exercises project. You will need to learn how to embed images in markdown files.

## SOLUTION

>OTHER LINUX COMMANDS

1. `cal`: shows this month's calendar.
![cal-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/cal_command.png)

2. `w` : shows the user that is online.
![w-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/w-command.png)

3. `free` : this command shows the free memory available (-h for human readable, -m for MB, -g for GB.). It also shows the used memory.
![free-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/free-command.png)

4. `lsof -u user` : lists all open files by the user.
![lsof--u-user-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/lsof--u-user-command.png)

5. `ip a` : display all network interfaces and internet protocol address.
![ip-a-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/ip-a-command.png)

6. `dig domain` : display domain name system information for domain
![dig-domain-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/dig-domain-command.png)

7. `last reboot` : show system reboot history.
![last-reboot-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/last-reboot-command.png)

8. `env` : this displays all environment variables.
![env-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/env-command.png)

9. `help` : displays help information
![help-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/help-command.png)

10. `sudo` : allows regular users to run programs with the security privileges of the superuser or root.
![sudo-commandOutput](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/Exercise2/images/sudo-command.png)
