#

![Altschool Africa](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/logos/AltSchool.svg)

![cloudComputing](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/logos/cloudComputing.gif)

## ABOUT

This repository contains all of my solutions to the assignments given to Cloud Engineering students at Altschool Africa's [School of Engineering](https://altschoolafrica.com/schools/engineering).

### MAY THE FORCE BE WITH YOU

![cloud](https://gitlab.com/tuyojr/cloudtasks/-/blob/main/logos/cloud.gif)
