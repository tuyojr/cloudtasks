#

![Altschool Africa](https://github.com/tuyojr/cloudTasks/blob/main/logos/AltSchool.svg)

## Exercise

You already have Github account, aso setup a GitLab account if you don’t have one already
You already have a altschool-cloud-exercises project, clone the project to your local system
Setup your name and email in Git’s global config

>Instruction:

``` GIT COMMANDS
# Submit the output of:
git config -l
git remote -v
git log
```
